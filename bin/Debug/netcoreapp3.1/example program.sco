title`Math Quiz
settextcolour`blue
setconsolecolour`black
clear

--`this is a comment
send`QUIZ:
send` 
send`Question 1: What is 1+1?
setstringinput`answer

--` check if the answer is correct
setboolcompare`didwin`^answer^`2
if`|^didwin^|send`Correct!|wait

--` if its wrong then say wrong
setboolcompare`wrong`^ifreturn^`False
if`|^wrong^|send`Wrong!|wait

send`Thank you for taking the quiz!
send`Press any key to continue...
wait
send`Now running login... (The password is password)
run`login.sco
send`login finished!
wait
--`Thanks for using scorpion