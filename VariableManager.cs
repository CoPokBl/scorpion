﻿using System.Collections.Generic;
using System.Linq;

namespace Scorpion {
    public class VariableManager {

        public static Dictionary<string, string> stringvars = new Dictionary<string, string>();
        public static Dictionary<string, bool> boolvars = new Dictionary<string, bool>();

        public static void setString(string varname, string varvalue) {
            if (stringvars.ContainsKey(varname)) {
                stringvars.Remove(varname);
            }
            stringvars.Add(varname, varvalue);
        }
        
        public static void setbool(string varname, bool varvalue) {
            if (boolvars.ContainsKey(varname)) {
                boolvars.Remove(varname);
            }
            boolvars.Add(varname, varvalue);
        }
        

        public static string getString(string varname) {
            return stringvars[varname];
        }
        
        public static bool getbool(string varname) {
            return boolvars[varname];
        }
        

        
        // interpreter variable substitution
        public static string ReplaceVars(string line) {

            string newline = line;
            
            for (int i = 0; i < stringvars.Count; i++) {
                newline = line.Replace("^" + stringvars.Keys.ToArray()[i] + "^", stringvars.Values.ToArray()[i]);
            }
            
            for (int i = 0; i < boolvars.Count; i++) {
                newline = newline.Replace("^" + boolvars.Keys.ToArray()[i].ToString() + "^", boolvars.Values.ToArray()[i].ToString());
            }
            
            return newline;
        }


    }
}