﻿using System;
using System.IO;

namespace Scorpion {
    class Program {

        public static string filepath;
        public static string[] filecontent;
        public static string version = "0.0.1";

        static void Main(string[] args) {
            
            // Init
            dataClass.InitDic();
            
            // file getter
            if (args.Length > 0) {
                filepath = args[0];
            }
            else {
                Interpreter.showDebug = true;
                LiveCMD.Run();
            }

            if (args.Length > 1) {
                if (args[1] == "debug") {
                    Interpreter.showDebug = true;
                }
                else {
                    Interpreter.showDebug = false;
                }
            }
            

            filecontent = File.ReadAllLines(filepath);
            
            bool error = Interpreter.interpret(filecontent);


        }
    }
}