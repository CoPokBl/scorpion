﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.IO;

namespace Scorpion {
    public class Proccess {

        public static bool ProccessLine(string line, int linenum) {


            bool hasran = false;

            string[] args = VariableManager.ReplaceVars(line).Split("`");
            

            // title cmd
            if (args[0] == "title") {
                if (args.Length > 1) {
                    Console.Title = args[1];
                }
                else {
                    Interpreter.ThrowException("Error at line " + linenum + ". title requires at least 2 args.", false);
                }

                hasran = true;
            }
            
            //set string to input
            if (args[0] == "setstringinput") {
                string input = Console.ReadLine();
                VariableManager.setString(args[1], input);
                hasran = true;
            }
            
            // comment
            if (args[0] == "--") {
                hasran = true;
            }
            
            // wait cmd
            if (args[0] == "wait") {
                Console.ReadKey(true);
                hasran = true;
            }
            
            
            // setstring cmd
            if (args[0] == "setstring") {
                VariableManager.setString(args[1], args[2]);
                hasran = true;
            }
            
            // setstring cmd
            if (args[0] == "setbool") {
                VariableManager.setbool(args[1], Interpreter.toBool(args[2]));
                hasran = true;
            }

            // set bool conpare
            if (args[0] == "setboolcompare") {
                VariableManager.setbool(args[1], args[2] == args[3]);
                hasran = true;
            }
            
            // send
            if (args[0] == "send") {
                Console.WriteLine(args[1]);
                hasran = true;
            }

            /*// get input and set var
            if (args[0] == "setvarinput") {
                VariableManager.setString(args[1], Console.ReadLine());
                //Console.WriteLine("person said: " + );
                hasran = true;
            }*/

            if (args[0] == "setconsolecolour") {
                Console.BackgroundColor = dataClass.ConsoleColors[args[1]];
                hasran = true;
            }

            if (args[0] == "settextcolour") {
                Console.ForegroundColor = dataClass.ConsoleColors[args[1]];
                hasran = true;
            }
            
            if (args[0] == "clear") {
                Console.Clear();
                hasran = true;
            }

            if (args[0] == "exec") {
                try {
                    Process e = Process.Start("\"" + args[1] + "\"");
                    
                }
                catch (Exception e) {
                    Console.WriteLine(e.Message);
                    Interpreter.ThrowException("Error while running file in command ~" + line + "~ in line " + linenum, false);
                }
                hasran = true;
            }
            
            // run other program
            if (args[0] == "run") {
                string[] filecontent;
                filecontent = File.ReadAllLines(args[1]);
                Interpreter.interpret(filecontent);
                hasran = true;
            }
            
            // skip to
            if (args[0] == "moveto") {
                Interpreter.needwait = true;
                Interpreter.waitfor = args[1];
                hasran = true;
            }
            
            // exit
            if (args[0] == "exit") {
                System.Environment.Exit(0);
            }

            if (args[0] == "if") {
                ProccessIf(line);
                hasran = true;
            }
            
            // goto line
            if (args[0] == "--+") {
                hasran = true;
            }
            

            // no cmd
            if (!hasran && line != "") {
                Interpreter.ThrowException("Invalid Command ~" + line + "~ at line " + linenum, false);
            }
            

            return false;
        }


        public static void ProccessIf(string line) {
            // sub in vars
            line = VariableManager.ReplaceVars(line);
            // remove if
            line = line.Remove(0, 3);
            // split cmds
            string[] cmds = line.Split("|");
            // if check
            if (cmds[1].ToLower() == "true") {
                // the condition is true

                for (int i = 2; i < cmds.Length; i++) {
                    // run cmds
                    ProccessLine(cmds[i], i);
                }
                VariableManager.setbool("ifreturn", true);
            }
            else {
                // condition false
                // SET IF RETURN to false
                VariableManager.setbool("ifreturn", false);
            }
        }
        
        
    }
}