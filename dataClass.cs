﻿using System;
using System.Collections.Generic;

namespace Scorpion {
    public class dataClass {

        public static Dictionary<string, ConsoleColor> ConsoleColors = new Dictionary<string, ConsoleColor>();


        public static void InitDic() {
            // console colours
            ConsoleColors.Add("blue", ConsoleColor.Blue);
            ConsoleColors.Add("black", ConsoleColor.Black);
            ConsoleColors.Add("red", ConsoleColor.Red);
            ConsoleColors.Add("green", ConsoleColor.Green);
            ConsoleColors.Add("grey", ConsoleColor.Gray);
            ConsoleColors.Add("white", ConsoleColor.White);



        }

    }
}