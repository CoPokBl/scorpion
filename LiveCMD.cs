﻿
using System;

namespace Scorpion {
    public class LiveCMD {
        public static bool ContinueEXE = true;
        public static ConsoleColor ccolour = Console.BackgroundColor;

        public static void Run() {
            // init stuff
            Console.WriteLine("Running Scorpion Version: " + Program.version);
            int cline = 0;
            
            // loops for cmds
            while (ContinueEXE) {

                cline++;
                Console.BackgroundColor = ConsoleColor.DarkBlue;
                string cmd = Console.ReadLine();
                Console.BackgroundColor = ccolour;
                

                Proccess.ProccessLine(cmd, cline);

            }
            
        }
        
    }
}