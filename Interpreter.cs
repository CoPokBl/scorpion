﻿using System;

namespace Scorpion {
    public class Interpreter {

        public static bool showDebug;
        public static bool interupt = false;
        
        // wait for
        public static string waitfor = "";
        public static bool needwait = false;

        public static bool interpret(string[] filecontent) {
            
            for (int i = 0; i < filecontent.Length; i++) {

                if (interupt) {
                    return false;
                }
                
                Proccess.ProccessLine(filecontent[i], i);
                
                // testing
                if (needwait) {
                    
                    // loop through each line and look for the bookmark
                    for (int j = 0; j < filecontent.Length; j++) {
                        
                        if (filecontent[j] == "--+`" + waitfor) {
                            i = j+1;
                            needwait = false;
                        }
                        
                    }

                    if (needwait) {
                        ThrowException("Cannot find the '" + waitfor + "' bookmark. Ignoring...", false);
                        needwait = false;
                    }
                    
                }
                
                
            }

            return true;
        }

        public static void ThrowException(string message, bool fatal) {

            if (fatal) {
                // error is fatal stop program
                Console.WriteLine("FATAL ERROR: " + message);
                interupt = true;
                // quit app
            }
            else {
                if (showDebug) {
                    ConsoleColor orginal = Console.BackgroundColor;
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.WriteLine("Error: " + message);
                    Console.WriteLine("Press any key to continue running...");
                    Console.ReadKey(true);
                    Console.BackgroundColor = orginal;
                }
            }
            
        }

        public static bool toBool(string data) {
            bool result;
            try {
                result = bool.Parse(data);
            }
            catch (Exception) {
                ThrowException("\"" + data + "\" is invalid boolean", true);
                return false;
            }


            return result;
        }
        
        
    }
}